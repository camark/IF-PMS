+function ($) {
    'use strict';
    $.fn.ifpager = function(options){
    	var defaults = {
    	        'type': 'POST',
    	        'url': '',
    	        'data': {},
    	        'timeout' : 30000,
    	        'size' : 10,
    	        'mask' : true
    	    };
    	var op = $.extend({}, defaults, options);
    	var $this = $(this);
    	var total = parseInt(op.total); //总记录数 
    	var size = parseInt(op.size); //每页显示数 
    	var pages = Math.floor(total / size); 
    	if (total % size != 0) { 
    		pages++; 
    	} 
    	laypage({
            cont: op.cont, //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
            pages: pages, //总页数
            curr: op.curr, //初始化当前页
            skip: true, //是否开启跳页
            skin: 'molv',
            jump: function(e, first){ //触发分页后的回调
            	var index = layer.load(0, {shade: false}); 
            	$.ajax({
        	        type     : op.type,
        	        url      : op.url,
        	        data     : $.extend({}, {
        	        	curr : e.curr,
        	        	size : op.size
        	        }, op.data),
        	        cache    : false,
        	        dataType : 'html',
        	        timeout  : op.timeout,
        	        error	 : function(e){
        	        	layer.close(index);
        	        	layer.msg('加载失败');
        	        },
        	        success  : op.success || function(e){
        	        	layer.close(index);
        	        	$this.html(e);
        	        }
        		});
            }
        });
		return this;
    }
}(jQuery)