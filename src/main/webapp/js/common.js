function del(id, url){
	var options = {
		id : id,
		confirm	: "确认删除选中的记录？",
		ajaxurl : url
	};
	selOneToRequest(options);
}

function dels(url){
	var options = {
		confirm	: "确认批量删除选中的记录？",
		ajaxurl : url
	};
	selMultiToRequest(options);
}

function pager(url, data, total, size){
	var options = {
        url: url,
        data: data,
        total : total,
        size : size
    };
	commonPager(options);
}

//验证并提交指定Form
function validSubmitForm(formId){
	$('#' + formId).Validform({
		tiptype : 2,
		ajaxPost : true,
		beforeSubmit : function(form){
			var index = layer.load(0, {shade: false}); 
			var options = {
				success : function(e){
					layer.close(index);
					layer.msg(e.msg);
				},
				error : function(e){
					layer.close(index);
					layer.msg('加载失败');
				}
			};
			$(form[0]).ajaxSubmit(options);
	        return false;  
	    }
	});
}

//验证并提交指定Form，然后刷新父级
function validSubmitFormAndRefreshParent(formId){
	$('#' + formId).Validform({
		tiptype : 2,
		ajaxPost : true,
		beforeSubmit : function(form){
			var index = layer.load(0, {shade: false}); 
			var options = {
				success : function(e){
					layer.close(index);
					if(e.code == 200){
						var parentIndex = parent.layer.getFrameIndex(window.name);
						parent.location.replace(parent.location.href);
						parent.layer.close(parentIndex);
					}
					else{
						layer.msg(e.msg);
					}
				},
				error : function(e){
					layer.close(index);
					layer.msg('加载失败');
				}
			};
			$(form[0]).ajaxSubmit(options);
	        return false;  
	    }
	});
}

//对单值进行请求，如果操作成功则刷新本页面
function selOneToRequest(options){
	var defaults = {
			id : "",
			confirm	: "确认操作选中的记录？",
			ajaxtype : "POST",
			ajaxurl : "",
			ajaxdata : {}
		};
	var op = $.extend({}, defaults, options);
	layer.confirm(op.confirm, {
	    btn: ['确定','取消'] //按钮
	}, function(){
		var ajaxOptions = {
    		type : op.ajaxtype,
    		dataType : 'json',
    		url : op.ajaxurl,
    		data : $.extend({}, op.ajaxdata, {id : op.id}),
	        error : function(e){
	        	layer.msg('请求错误');
	        },
	        success : function(e){
	        	if(e.code == 200){
	        		window.location.replace(window.location.href);
	        	}
	        	else{
	        		layer.msg('操作失败');
	        	}
	        }
    	};
    	commmonAjax(ajaxOptions);
	}, function(){
	    
	});
}

//选中多个复选框的值进行请求，如果成功则刷新本页面
function selMultiToRequest(options){
	var defaults = {
		chkname	: "chkId",
		confirm	: "确认批量操作选中的记录？",
		ajaxtype : "POST",
		ajaxurl : "",
		ajaxdata : {}
	};
	var op = $.extend({}, defaults, options);
	var checkedNum = $("input[name='" + op.chkname + "']:checked").length; 
	if(checkedNum == 0) { 
		layer.msg("请选择至少一项！"); 
		return; 
	}
	layer.confirm(op.confirm, {
	    btn: ['确定','取消'] //按钮
	}, function(){
		var checkedList = new Array(); 
		$("input[name='" + op.chkname + "']:checked").each(function() { 
			checkedList.push($(this).val()); 
		});
		var ajaxOptions = {
    		type : op.ajaxtype,
    		dataType : 'json',
    		url : op.ajaxurl,
    		data : $.extend({}, op.ajaxdata, {ids : checkedList.toString()}),
	        error : function(e){
	        	layer.msg('请求错误');
	        },
	        success : function(e){
	        	if(e.code == 200){
	        		window.location.replace(window.location.href);
	        	}
	        	else{
	        		layer.msg('操作失败');
	        	}
	        }
    	};
    	commmonAjax(ajaxOptions);
	}, function(){
	    
	});
}

//分页处理
function commonPager(options){
	var defaults = {
			datacont : 'dataContent',
			cont : 'pager',
	        type : 'POST',
	        url : '',
	        data : {},
	        timeout : 30000,
	        total : 0,
	        size : 10,
	        mask : true
	    };
	var op = $.extend({}, defaults, options);
	var total = parseInt(op.total); //总记录数 
	var size = parseInt(op.size); //每页显示数 
	var pages = Math.floor(total / size); 
	if (total % size != 0) { 
		pages++; 
	} 
	laypage({
        cont: op.cont, //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
        pages: pages, //总页数
        curr: op.curr, //初始化当前页
        skip: true, //是否开启跳页
        skin: 'molv',
        jump: function(e, first){ //触发分页后的回调
        	var ajaxOptions = {
        		type : op.type,
        		dataType : 'html',
        		url : op.url,
        		data : $.extend({}, {
    	        	curr : e.curr,
    	        	size : op.size
    	        }, op.data),
    	        error : op.error || function(e){
    	        	layer.msg('请求错误');
    	        },
    	        success : op.success || function(e){
    	        	$("#" + op.datacont).html(e);
    	        }
        	};
        	commmonAjax(ajaxOptions);
        }
    });
}

//通用的ajax方法
function commmonAjax(options){
	var defaults = {
		type : 'POST',
		cache : false,
		dataType : 'json',
		timeout : 30000,
	};
	var op = $.extend({}, defaults, options);
	var index = layer.load(0, {shade: false}); 
	$.ajax({
        type     : op.type,
        url      : op.url,
        data     : op.data || {},
        cache    : op.cache,
        dataType : op.dataType,
        timeout  : op.timeout,
        error	 : op.error || function(e){
        	layer.close(index);
        	if(typeof op.error == 'function'){
        		op.error(e);
        	}
        },
        success  : function(e){
        	layer.close(index);
        	if(typeof op.success == 'function'){
        		op.success(e);
        	}
        }
	});
}
