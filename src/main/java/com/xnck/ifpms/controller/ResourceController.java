package com.xnck.ifpms.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xnck.ifpms.entity.Resource;
import com.xnck.ifpms.entity.ResourceType;
import com.xnck.ifpms.model.TreeNode;
import com.xnck.ifpms.service.ResourceService;

/**
 * 资源视图控制器
 * @author zhangmengliang
 *
 */
@Controller
@RequestMapping(value="/res")
public class ResourceController extends BaseController{
	
	private static Logger log = Logger.getLogger(ActionController.class);

	@Autowired
	private ResourceService resourceService;
	
	/**
	 * 查找资源
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView getResources(){
		ModelAndView mv = new ModelAndView("/resource/search");
		try {
			Integer total = resourceService.getResourcesCount(null, null);
			mv.addObject("total", total);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 查找资源
	 * @param resourceName
	 * @param remark
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ModelAndView postResources(String resourceName, String remark){
		Integer total = resourceService.getResourcesCount(resourceName, remark);
		ModelAndView mv = new ModelAndView("/resource/search");
		mv.addObject("total", total);
		mv.addObject("resourceName", resourceName);
		mv.addObject("remark", remark);
		return mv;
	}
	
	/**
	 * 获得资源列表
	 * @param response
	 * @param resourceName
	 * @param remark
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public ModelAndView postResources(HttpServletResponse response, 
			String resourceName, String remark, Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/resource/list");
		try {
			List<Resource> resources = resourceService.getResources(resourceName, remark, curr, size);
			mv.addObject("resources", resources);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 创建资源
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView getCreate(){
		ModelAndView mv = new ModelAndView("/resource/create");
		try {
			List<ResourceType> types = resourceService.getAllResourceTypes();
			mv.addObject("types", types);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 创建资源
	 * @param request
	 * @param response
	 * @param resourceCode
	 * @param resourceName
	 * @param remark
	 * @param resourceType
	 * @param resourceEnable
	 * @param viewName
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void postCreate(HttpServletRequest request, HttpServletResponse response,
			String resourceCode, String resourceName, String remark, String resourceType,
			Boolean resourceEnable, String viewName){
		try {
			resourceService.saveResource(null, resourceCode, resourceName, resourceType, remark, 
					resourceEnable, null, viewName);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, e.toString());
		}
		
	}
	
	/**
	 * 修改资源
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
			String id){
		ModelAndView mv = new ModelAndView("/resource/update");
		try {
			Resource resource = resourceService.getResource(id);
			mv.addObject("resource", resource);
			List<ResourceType> types = resourceService.getAllResourceTypes();
			mv.addObject("types", types);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 修改资源
	 * @param request
	 * @param response
	 * @param id
	 * @param resourceCode
	 * @param resourceName
	 * @param remark
	 * @param resourceType
	 * @param resourceEnable
	 * @param viewName
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public void postUpdate(HttpServletRequest request, HttpServletResponse response,
			String id, String resourceCode, String resourceName, String remark, String resourceType,
			Boolean resourceEnable, String viewName){
		try {
			resourceService.saveResource(id, resourceCode, resourceName, resourceType, remark, 
					resourceEnable, null, viewName);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, e.toString());
		}
	}
	
	/**
	 * 删除资源
	 * @param request
	 * @param response
	 * @param id
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public void postDelete(HttpServletRequest request, HttpServletResponse response,
			String id){
		try {
			resourceService.delResource(id);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, e.toString());
		}
	}
	
	/**
	 * 批量删除资源
	 * @param request
	 * @param response
	 * @param ids
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/batchdelete", method=RequestMethod.POST)
	public void postBatchDelete(HttpServletRequest request, HttpServletResponse response,
			String ids){
		try {
			resourceService.delResources(ids);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, e.toString());
		}
	}
	
	/**
	 * 按资源查询权限
	 * @param request
	 * @param response
	 * @param resId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionsearch", method=RequestMethod.GET)
	public ModelAndView getActionSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="rid")String resId){
		ModelAndView mv = new ModelAndView("/resource/actionsearch");
		mv.addObject("resId", resId);
		return mv;
	}
	
	/**
	 * 权限子节点
	 * @param request
	 * @param response
	 * @param parentId
	 * @param resId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionchilds", method=RequestMethod.POST)
	public void postActionChilds(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id")String parentId,
			@RequestParam(value="rid")String resId){
		List<TreeNode> nodes;
		try {
			nodes = resourceService.getActionTreeNodes(parentId, resId);
		} catch (Exception e) {
			log.error(e.toString());
			nodes = new ArrayList<TreeNode>();
		}
		respSuccessMsg(response, nodes, "ok");
	}
	
	/**
	 * 关联资源和权限
	 * @param request
	 * @param response
	 * @param actionIdsString
	 * @param resId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/selactions", method=RequestMethod.POST)
	public void postSelActions(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="ids")String actionIdsString,
			@RequestParam(value="rid")String resId){
		try {
			resourceService.saveSelActions(resId, actionIdsString);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
}
