package com.xnck.ifpms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xiaoleilu.hutool.StrUtil;
import com.xnck.ifpms.constant.ContextConstant;
import com.xnck.ifpms.service.AccountService;
import com.xnck.ifpms.utils.SessionUtils;

/**
 * 根目录视图控制
 * @author zhangmengliang
 *
 */
@Controller
@RequestMapping(value="/")
public class RootController {

	@Autowired
	private AccountService accountService;
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public ModelAndView getIndex(){
		ModelAndView mv = new ModelAndView("/index");
		return mv;
	}
	
	@RequestMapping(value="/welcome", method=RequestMethod.GET)
	public ModelAndView getIndexRight(){
		ModelAndView mv = new ModelAndView("/welcome");
		return mv;
	}
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView getLogin(){
		ModelAndView mv = new ModelAndView("/login");
		return mv;
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String postLogin(HttpServletRequest request, HttpServletResponse response, 
			String loginName, String loginPwd){
		String userId = accountService.getUserId(loginName, loginPwd);
		if (StrUtil.isNotBlank(userId)) {
			SessionUtils.setAttr(request, ContextConstant.IDEN_CERT_KEY, userId);
			return "redirect:/index.html";
		}
		return "/login";
	}
}
