package com.xnck.ifpms.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xnck.ifpms.entity.Action;
import com.xnck.ifpms.entity.Resource;
import com.xnck.ifpms.entity.Role;
import com.xnck.ifpms.model.TreeNode;
import com.xnck.ifpms.service.ActionService;
import com.xiaoleilu.hutool.StrUtil;

/**
 * 权限视图控制
 * @author zhangmengliang
 *
 */
@Controller
@RequestMapping(value="/action")
public class ActionController extends BaseController{
	private static Logger log = Logger.getLogger(ActionController.class);

	@Autowired
	private ActionService actionService;
	
	/**
	 * 查找权限
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView getActions(){
		ModelAndView mv = new ModelAndView("/action/search");
		try {
			Integer total = actionService.getActionsCount(null, null);
			mv.addObject("total", total);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 查找权限
	 * @param actionName
	 * @param remark
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ModelAndView postActions(String actionName, String remark){
		Integer total = actionService.getActionsCount(actionName, remark);
		ModelAndView mv = new ModelAndView("/action/search");
		mv.addObject("total", total);
		mv.addObject("actionName", actionName);
		mv.addObject("remark", remark);
		return mv;
	}
	
	/**
	 * 获取权限列表
	 * @param response
	 * @param parentId
	 * @param actionName
	 * @param remark
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public ModelAndView postActions(HttpServletResponse response,
			@RequestParam(value="parentId", required=false)String parentId,
			String actionName, String remark, Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/action/list");
		try {
			List<Action> actions = actionService.getActions(parentId, actionName, remark, curr, size);
			mv.addObject("actions", actions);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 获取子权限
	 * @param request
	 * @param response
	 * @param parentId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/childs", method=RequestMethod.POST)
	public void postChilds(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id", required=false)String parentId){
		List<TreeNode> nodes;
		try {
			nodes = actionService.getActionNodesByParentId(parentId);
		} catch (Exception e) {
			log.error(e.toString());
			nodes = new ArrayList<TreeNode>();
		}
		respSuccessMsg(response, nodes, "ok");
	}
	
	/**
	 * 创建权限
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView getCreate(){
		ModelAndView mv = new ModelAndView("/action/create");
		return mv;
	}
	
	/**
	 * 创建权限
	 * @param request
	 * @param response
	 * @param actionName
	 * @param parentId
	 * @param remark
	 * @param actionEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void getCreate(HttpServletRequest request, HttpServletResponse response,
			String actionName, String parentId, String remark, Boolean actionEnable){
		try {
			actionService.saveAction(null, parentId, actionName, remark, actionEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
	
	/**
	 * 修改权限
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
			String id){
		ModelAndView mv = new ModelAndView("/action/update");
		try {
			Action action = actionService.getAction(id);
			mv.addObject("action", action);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 修改权限
	 * @param request
	 * @param response
	 * @param id
	 * @param parentId
	 * @param actionName
	 * @param remark
	 * @param actionEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public void postUpdate(HttpServletRequest request, HttpServletResponse response,
			String id, String parentId, String actionName, String remark, Boolean actionEnable){
		try {
			actionService.saveAction(id, parentId, actionName, remark, actionEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 删除权限
	 * @param request
	 * @param response
	 * @param id
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public void postDelete(HttpServletRequest request, HttpServletResponse response,
			String id){
		try {
			actionService.delAction(id);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 批量删除权限
	 * @param request
	 * @param response
	 * @param ids
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/batchdelete", method=RequestMethod.POST)
	public void postBatchDelete(HttpServletRequest request, HttpServletResponse response,
			String ids){
		try {
			actionService.delActions(ids);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 按权限获取角色
	 * @param request
	 * @param response
	 * @param actionId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolesearch", method=RequestMethod.GET)
	public ModelAndView getRoleSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="aid")String actionId){
		ModelAndView mv = new ModelAndView("/action/rolesearch");
		Integer total = -1;
		try {	
			total = actionService.getCountRoleForAction(actionId, null);
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("selFlag", "1");
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 按权限获取角色
	 * @param request
	 * @param response
	 * @param selFlag
	 * @param roleName
	 * @param actionId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolesearch", method=RequestMethod.POST)
	public ModelAndView postRoleSearch(HttpServletRequest request, HttpServletResponse response,
			String selFlag, String roleName,
			@RequestParam(value="aid")String actionId){
		//如果标识值为空则默认为1,1代表已关联
		if (StrUtil.isBlank(selFlag)) {
			selFlag = "1";
		}
		ModelAndView mv = new ModelAndView("/action/rolesearch");
		Integer total = -1;
		try {
			//1时查询已关联的角色，其它情况下查询未关联的角色
			if ("1".equals(selFlag)) {
				total = actionService.getCountRoleForAction(actionId, roleName);
			}
			else {
				total = actionService.getCountRoleNotForAction(actionId, roleName);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("roleName", roleName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 按权限分页获取关联的角色
	 * @param response
	 * @param selFlag
	 * @param roleName
	 * @param actionId
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolelist", method=RequestMethod.POST)
	public ModelAndView postRoleList(HttpServletResponse response, 
			String selFlag, String roleName, String actionId,
			Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/action/rolelist");
		List<Role> roles;
		try {
			//1时查询已关联的角色，其它情况下查询未关联的角色
			if ("1".equals(selFlag)) {
				roles = actionService.getRoleForAction(actionId, roleName, curr, size);
			}
			else {
				roles = actionService.getRoleNotForAction(actionId, roleName, curr, size);
			}
		} catch (Exception e) {
			log.error(e.toString());
			roles = new ArrayList<Role>();
		}
		mv.addObject("roles", roles);
		mv.addObject("roleName", roleName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 批量关联权限和角色
	 * @param request
	 * @param response
	 * @param actionId
	 * @param roleIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="setroles", method=RequestMethod.POST)
	public void postSetRoles(HttpServletRequest request, HttpServletResponse response,
			String actionId, @RequestParam(value="ids")String roleIds){
		try {
			actionService.setActionRoles(actionId, roleIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 批量移除权限和角色
	 * @param request
	 * @param response
	 * @param actionId
	 * @param roleIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="removeroles", method=RequestMethod.POST)
	public void postRemoveRoles(HttpServletRequest request, HttpServletResponse response,
			String actionId, @RequestParam(value="ids")String roleIds){
		try {
			actionService.removeActionRoles(actionId, roleIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 关联权限和资源
	 * @param request
	 * @param response
	 * @param actionId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/ressearch", method=RequestMethod.GET)
	public ModelAndView getResSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="aid")String actionId){
		ModelAndView mv = new ModelAndView("/action/ressearch");
		Integer total = -1;
		try {	
			total = actionService.getCountResourceForAction(actionId, null);
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("selFlag", "1");
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 关联权限和资源
	 * @param request
	 * @param response
	 * @param selFlag
	 * @param resName
	 * @param actionId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/ressearch", method=RequestMethod.POST)
	public ModelAndView postResSearch(HttpServletRequest request, HttpServletResponse response,
			String selFlag, String resName,
			@RequestParam(value="aid")String actionId){
		//1为已关联
		if (StrUtil.isBlank(selFlag)) {
			selFlag = "1";
		}
		ModelAndView mv = new ModelAndView("/action/ressearch");
		Integer total = -1;
		try {
			//1时查询已关联的资源，其它情况下查询未关联的资源
			if ("1".equals(selFlag)) {
				total = actionService.getCountResourceForAction(actionId, resName);
			}
			else {
				total = actionService.getCountResourceNotForAction(actionId, resName);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("resName", resName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 获得和权限关联的资源列表
	 * @param response
	 * @param selFlag
	 * @param resName
	 * @param actionId
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/reslist", method=RequestMethod.POST)
	public ModelAndView postResList(HttpServletResponse response, 
			String selFlag, String resName, String actionId,
			Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/action/reslist");
		List<Resource> resources;
		try {	
			//1时查询已关联的资源，其它情况下查询未关联的资源
			if ("1".equals(selFlag)) {
				resources = actionService.getResourceForAction(actionId, resName, curr, size);
			}
			else {
				resources = actionService.getResourceNotForAction(actionId, resName, curr, size);
			}
		} catch (Exception e) {
			log.error(e.toString());
			resources = new ArrayList<Resource>();
		}
		mv.addObject("resources", resources);
		mv.addObject("resName", resName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("actionId", actionId);
		return mv;
	}
	
	/**
	 * 关联权限和资源
	 * @param request
	 * @param response
	 * @param actionId
	 * @param resIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="setresources", method=RequestMethod.POST)
	public void postSetResources(HttpServletRequest request, HttpServletResponse response,
			String actionId, @RequestParam(value="ids")String resIds){
		try {
			actionService.setActionResources(actionId, resIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 移除权限和资源
	 * @param request
	 * @param response
	 * @param actionId
	 * @param resIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="removeresources", method=RequestMethod.POST)
	public void postRemoveResources(HttpServletRequest request, HttpServletResponse response,
			String actionId, @RequestParam(value="ids")String resIds){
		try {
			actionService.removeActionResources(actionId, resIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
}
