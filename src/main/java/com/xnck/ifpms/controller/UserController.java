package com.xnck.ifpms.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xnck.ifpms.entity.Account;
import com.xnck.ifpms.entity.Role;
import com.xnck.ifpms.entity.User;
import com.xnck.ifpms.model.TreeNode;
import com.xnck.ifpms.service.UserService;
import com.xiaoleilu.hutool.StrUtil;

/**
 * 人员视图控制
 * @author zhangmengliang
 *
 */
@Controller
@RequestMapping(value="/user")
public class UserController extends BaseController{
	
	private static Logger log = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	
	/**
	 * 查找人员
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView getUsers(){
		ModelAndView mv = new ModelAndView("/user/search");
		try {
			Integer usersCount = userService.getUsersCount(null);
			mv.addObject("total", usersCount);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 查找人员
	 * @param userName
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ModelAndView postUsers(String userName){
		Integer usersCount = userService.getUsersCount(userName);
		ModelAndView mv = new ModelAndView("/user/search");
		mv.addObject("total", usersCount);
		mv.addObject("userName", userName);
		return mv;
	}
	
	/**
	 * 人员列表
	 * @param response
	 * @param userName
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public ModelAndView postUsers(HttpServletResponse response, 
			String userName, Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/user/list");
		try {
			List<User> users = userService.getUsers(userName, curr, size);
			mv.addObject("users", users);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 创建人员
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView getCreate(){
		ModelAndView mv = new ModelAndView("/user/create");
		return mv;
	}
	
	/**
	 * 创建人员
	 * @param request
	 * @param response
	 * @param userName
	 * @param userEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void getCreate(HttpServletRequest request, HttpServletResponse response,
			String userName, Boolean userEnable){
		try {
			userService.saveUser(null, userName, userEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
	
	/**
	 * 修改人员
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
			String id){
		ModelAndView mv = new ModelAndView("/user/update");
		try {
			User user = userService.getUser(id);
			mv.addObject("user", user);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 修改人员
	 * @param request
	 * @param response
	 * @param id
	 * @param userName
	 * @param userEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public void postUpdate(HttpServletRequest request, HttpServletResponse response,
			String id, String userName, Boolean userEnable){
		try {
			userService.saveUser(id, userName, userEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 删除人员
	 * @param request
	 * @param response
	 * @param id
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public void postDelete(HttpServletRequest request, HttpServletResponse response,
			String id){
		try {
			userService.delUser(id);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 批量删除人员
	 * @param request
	 * @param response
	 * @param ids
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/batchdelete", method=RequestMethod.POST)
	public void postBatchDelete(HttpServletRequest request, HttpServletResponse response,
			String ids){
		try {
			userService.delUsers(ids);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 按人员查找角色
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolesearch", method=RequestMethod.GET)
	public ModelAndView getRoleSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="uid")String userId){
		ModelAndView mv = new ModelAndView("/user/rolesearch");
		Integer total = -1;
		try {	
			total = userService.getCountRoleForUser(userId, null);
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("selFlag", "1");
		mv.addObject("userId", userId);
		return mv;
	}
	
	/**
	 * 按人员查找角色
	 * @param request
	 * @param response
	 * @param selFlag
	 * @param roleName
	 * @param userId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolesearch", method=RequestMethod.POST)
	public ModelAndView postRoleSearch(HttpServletRequest request, HttpServletResponse response,
			String selFlag, String roleName,
			@RequestParam(value="uid")String userId){
		if (StrUtil.isBlank(selFlag)) {
			selFlag = "1";
		}
		ModelAndView mv = new ModelAndView("/user/rolesearch");
		Integer total = -1;
		try {	
			if ("1".equals(selFlag)) {
				total = userService.getCountRoleForUser(userId, roleName);
			}
			else {
				total = userService.getCountRoleNotForUser(userId, roleName);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("roleName", roleName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("userId", userId);
		return mv;
	}
	
	/**
	 * 与人员相关的角色列表
	 * @param response
	 * @param selFlag
	 * @param roleName
	 * @param userId
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/rolelist", method=RequestMethod.POST)
	public ModelAndView postRoleList(HttpServletResponse response, 
			String selFlag, String roleName, String userId,
			Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/user/rolelist");
		List<Role> roles;
		try {	
			if ("1".equals(selFlag)) {
				roles = userService.getRoleForUser(userId, roleName, curr, size);
			}
			else {
				roles = userService.getRoleNotForUser(userId, roleName, curr, size);
			}
		} catch (Exception e) {
			log.error(e.toString());
			roles = new ArrayList<Role>();
		}
		mv.addObject("roles", roles);
		mv.addObject("roleName", roleName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("userId", userId);
		return mv;
	}
	
	/**
	 * 关联人员和角色
	 * @param request
	 * @param response
	 * @param userId
	 * @param roleIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="setroles", method=RequestMethod.POST)
	public void postSetRoles(HttpServletRequest request, HttpServletResponse response,
			String userId, @RequestParam(value="ids")String roleIds){
		try {
			userService.setUserRoles(userId, roleIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 移除人员和角色的关联
	 * @param request
	 * @param response
	 * @param userId
	 * @param roleIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="removeroles", method=RequestMethod.POST)
	public void postRemoveRoles(HttpServletRequest request, HttpServletResponse response,
			String userId, @RequestParam(value="ids")String roleIds){
		try {
			userService.removeUserRoles(userId, roleIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 按人员查找权限
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionsearch", method=RequestMethod.GET)
	public ModelAndView getActionSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="uid")String userId){
		ModelAndView mv = new ModelAndView("/user/actionsearch");
		mv.addObject("userId", userId);
		return mv;
	}
	
	/**
	 * 子权限
	 * @param request
	 * @param response
	 * @param parentId
	 * @param userId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionchilds", method=RequestMethod.POST)
	public void postActionChilds(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id")String parentId,
			@RequestParam(value="uid")String userId){
		List<TreeNode> nodes;
		try {
			nodes = userService.getActionTreeNodes(parentId, userId);
		} catch (Exception e) {
			log.error(e.toString());
			nodes = new ArrayList<TreeNode>();
		}
		respSuccessMsg(response, nodes, "ok");
	}
	
	/**
	 * 关联人员和权限
	 * @param request
	 * @param response
	 * @param actionIdsString
	 * @param userId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/selactions", method=RequestMethod.POST)
	public void postSelActions(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="ids")String actionIdsString,
			@RequestParam(value="uid")String userId){
		try {
			userService.saveSelActions(userId, actionIdsString);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
	
	/**
	 * 设置登录信息
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/setaccount", method=RequestMethod.GET)
	public ModelAndView getSetAccount(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="uid")String userId){
		ModelAndView mv = new ModelAndView("/user/setaccount");
		try {
			Account account = userService.getAccount(userId);
			mv.addObject("account", account);
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("userId", userId);
		return mv;
	}
	
	/**
	 * 设置登录信息
	 * @param request
	 * @param response
	 * @param userId
	 * @param loginName
	 * @param loginPwd
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/setaccount", method=RequestMethod.POST)
	public void getSetAccount(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="uid")String userId, String loginName, String loginPwd){
		try {
			userService.saveAccount(userId, loginName, loginPwd);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
}
