package com.xnck.ifpms.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.xnck.ifpms.entity.Role;
import com.xnck.ifpms.entity.User;
import com.xnck.ifpms.model.TreeNode;
import com.xnck.ifpms.service.RoleService;
import com.xiaoleilu.hutool.StrUtil;

/**
 * 角色视图控制
 * @author zhangmengliang
 *
 */
@Controller
@RequestMapping(value="/role")
public class RoleController extends BaseController{
	private static Logger log = Logger.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;
	
	/**
	 * 查找角色
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public ModelAndView getRoles(){
		ModelAndView mv = new ModelAndView("/role/search");
		try {
			Integer total = roleService.getRolesCount(null);
			mv.addObject("total", total);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 查找角色
	 * @param roleName
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ModelAndView postRoles(String roleName){
		Integer total = roleService.getRolesCount(roleName);
		ModelAndView mv = new ModelAndView("/role/search");
		mv.addObject("total", total);
		mv.addObject("roleName", roleName);
		return mv;
	}
	
	/**
	 * 角色列表
	 * @param response
	 * @param roleName
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public ModelAndView postRoles(HttpServletResponse response, 
			String roleName, Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/role/list");
		try {
			List<Role> roles = roleService.getRoles(roleName, curr, size);
			mv.addObject("roles", roles);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 创建角色
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ModelAndView getCreate(){
		ModelAndView mv = new ModelAndView("/role/create");
		return mv;
	}
	
	/**
	 * 创建角色
	 * @param request
	 * @param response
	 * @param roleName
	 * @param roleEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public void getCreate(HttpServletRequest request, HttpServletResponse response,
			String roleName, Boolean roleEnable){
		try {
			roleService.saveRole(null, roleName, roleEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
	
	/**
	 * 修改角色
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public ModelAndView getUpdate(HttpServletRequest request, HttpServletResponse response,
			String id){
		ModelAndView mv = new ModelAndView("/role/update");
		try {
			Role role = roleService.getRole(id);
			mv.addObject("role", role);
		} catch (Exception e) {
			log.error(e.toString());
		}
		return mv;
	}
	
	/**
	 * 修改角色
	 * @param request
	 * @param response
	 * @param id
	 * @param roleName
	 * @param roleEnable
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public void postUpdate(HttpServletRequest request, HttpServletResponse response,
			String id, String roleName, Boolean roleEnable){
		try {
			roleService.saveRole(id, roleName, roleEnable, null);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 删除角色
	 * @param request
	 * @param response
	 * @param id
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public void postDelete(HttpServletRequest request, HttpServletResponse response,
			String id){
		try {
			roleService.delRole(id);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 批量删除角色
	 * @param request
	 * @param response
	 * @param ids
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/batchdelete", method=RequestMethod.POST)
	public void postBatchDelete(HttpServletRequest request, HttpServletResponse response,
			String ids){
		try {
			roleService.delRoles(ids);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 按角色查找关联人员
	 * @param request
	 * @param response
	 * @param roleId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/usersearch", method=RequestMethod.GET)
	public ModelAndView getUserSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="rid")String roleId){
		ModelAndView mv = new ModelAndView("/role/usersearch");
		Integer total = -1;
		try {	
			total = roleService.getCountUserForRole(roleId, null);
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("selFlag", "1");
		mv.addObject("roleId", roleId);
		return mv;
	}
	
	/**
	 * 按角色查找关联人员
	 * @param request
	 * @param response
	 * @param selFlag
	 * @param userName
	 * @param roleId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/usersearch", method=RequestMethod.POST)
	public ModelAndView postUserSearch(HttpServletRequest request, HttpServletResponse response,
			String selFlag, String userName,
			@RequestParam(value="rid")String roleId){
		if (StrUtil.isBlank(selFlag)) {
			selFlag = "1";
		}
		ModelAndView mv = new ModelAndView("/role/usersearch");
		Integer total = -1;
		try {	
			if ("1".equals(selFlag)) {
				total = roleService.getCountUserForRole(roleId, userName);
			}
			else {
				total = roleService.getCountUserNotForRole(roleId, userName);
			}
		} catch (Exception e) {
			log.error(e.toString());
		}
		mv.addObject("total", total);
		mv.addObject("userName", userName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("roleId", roleId);
		return mv;
	}
	
	/**
	 * 与角色关联的人员列表
	 * @param response
	 * @param selFlag
	 * @param userName
	 * @param roleId
	 * @param curr
	 * @param size
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/userlist", method=RequestMethod.POST)
	public ModelAndView postUserList(HttpServletResponse response, 
			String selFlag, String userName, String roleId,
			Integer curr, Integer size){
		ModelAndView mv = new ModelAndView("/role/userlist");
		List<User> users;
		try {	
			if ("1".equals(selFlag)) {
				users = roleService.getUserForRole(roleId, userName, curr, size);
			}
			else {
				users = roleService.getUserNotForRole(roleId, userName, curr, size);
			}
		} catch (Exception e) {
			log.error(e.toString());
			users = new ArrayList<User>();
		}
		mv.addObject("users", users);
		mv.addObject("userName", userName);
		mv.addObject("selFlag", selFlag);
		mv.addObject("roleId", roleId);
		return mv;
	}
	
	/**
	 * 关联角色和人员
	 * @param request
	 * @param response
	 * @param roleId
	 * @param userIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="setusers", method=RequestMethod.POST)
	public void postSetUsers(HttpServletRequest request, HttpServletResponse response,
			String roleId, @RequestParam(value="ids")String userIds){
		try {
			roleService.setUserRoles(roleId, userIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 移除角色和人员关联
	 * @param request
	 * @param response
	 * @param roleId
	 * @param userIds
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="removeusers", method=RequestMethod.POST)
	public void postRemoveRoles(HttpServletRequest request, HttpServletResponse response,
			String roleId, @RequestParam(value="ids")String userIds){
		try {
			roleService.removeUserRoles(roleId, userIds);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
	}
	
	/**
	 * 按角色查询权限
	 * @param request
	 * @param response
	 * @param roleId
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionsearch", method=RequestMethod.GET)
	public ModelAndView getActionSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="rid")String roleId){
		ModelAndView mv = new ModelAndView("/role/actionsearch");
		mv.addObject("roleId", roleId);
		return mv;
	}
	
	/**
	 * 子权限
	 * @param request
	 * @param response
	 * @param parentId
	 * @param roleId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/actionchilds", method=RequestMethod.POST)
	public void postActionChilds(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="id")String parentId,
			@RequestParam(value="rid")String roleId){
		List<TreeNode> nodes;
		try {
			nodes = roleService.getActionTreeNodes(parentId, roleId);
		} catch (Exception e) {
			log.error(e.toString());
			nodes = new ArrayList<TreeNode>();
		}
		respSuccessMsg(response, nodes, "ok");
	}
	
	/**
	 * 关联角色和权限
	 * @param request
	 * @param response
	 * @param actionIdsString
	 * @param roleId
	 * @author:zhangmengliang
	 * @date: 2015年11月15日
	 */
	@RequestMapping(value="/selactions", method=RequestMethod.POST)
	public void postSelActions(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="ids")String actionIdsString,
			@RequestParam(value="rid")String roleId){
		try {
			roleService.saveSelActions(roleId, actionIdsString);
			respSuccessMsg(response, null, "ok");
		} catch (Exception e) {
			log.error(e.toString());
			respErrorMsg(response, "fail");
		}
		
	}
}
