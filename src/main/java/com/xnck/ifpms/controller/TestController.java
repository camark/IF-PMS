package com.xnck.ifpms.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xiaoleilu.hutool.StrUtil;
import com.xnck.ifpms.annotation.AuthenPassport;
import com.xnck.ifpms.annotation.Permission;
import com.xnck.ifpms.constant.ContextConstant;
import com.xnck.ifpms.service.AccountService;
import com.xnck.ifpms.utils.SessionUtils;

@Controller
@RequestMapping("/test")
public class TestController extends BaseController{
	
	@Autowired
	private AccountService accountService;

	@AuthenPassport
	@RequestMapping(value="/testlogin", method=RequestMethod.GET)
	public ModelAndView getTestLogin(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("/test/testlogin");
		return mv;
	}
	
	@Permission("url_test_testpermission")
	@RequestMapping(value="/testpermission", method=RequestMethod.GET)
	public ModelAndView getTestPermission(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("/test/testpermission");
		return mv;
	}
	
	@RequestMapping(value="/changeuser", method=RequestMethod.GET)
	public ModelAndView getChangeUser(){
		ModelAndView mv = new ModelAndView("/test/changeuser");
		return mv;
	}
	
	@RequestMapping(value="/changeuser", method=RequestMethod.POST)
	public void postChangeUser(HttpServletRequest request, HttpServletResponse response, 
			String loginName, String loginPwd){
		String userId = accountService.getUserId(loginName, loginPwd);
		if (StrUtil.isNotBlank(userId)) {
			SessionUtils.setAttr(request, ContextConstant.IDEN_CERT_KEY, userId);
			respSuccessMsg(response, null, "ok");
		}
		else {
			respErrorMsg(response, "fail");
		}
	}
}
