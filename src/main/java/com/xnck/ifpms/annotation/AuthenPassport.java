package com.xnck.ifpms.annotation;

import java.lang.annotation.*;

/**
 * 登录验证注解
 * @author zhangmengliang
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthenPassport {
	boolean validate() default true;
}
