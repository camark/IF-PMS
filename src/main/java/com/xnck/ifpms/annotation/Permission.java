package com.xnck.ifpms.annotation;

import java.lang.annotation.*;

/**
 * 权限验证注解
 * @author zhangmengliang
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Permission {
	/** 资源为URL类型的Code **/
	String value();
}
