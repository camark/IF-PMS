package com.xnck.ifpms.service;

import java.util.List;

import org.nutz.dao.Cnd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnck.ifpms.dao.AccountDao;
import com.xnck.ifpms.entity.Account;

/**
 * 登录账户信息业务逻辑
 * @author zhangmengliang
 *
 */
@Service
public class AccountService {

	@Autowired
	private AccountDao accountDao;
	
	/**
	 * 根据用户名和密码获得人员ID
	 * @param loginName 用户名
	 * @param loginPwd 用户密码
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月4日
	 */
	public String getUserId(String loginName, String loginPwd){
		List<Account> accounts = accountDao.search(
				Cnd.where(Account.FIELD_ACCOUNT, "=", loginName).and(Account.FIELD_PWD, "=", loginPwd));
		if (null == accounts || accounts.size() == 0) {
			return null;
		}
		return accounts.get(0).getUserid();
	}
}
