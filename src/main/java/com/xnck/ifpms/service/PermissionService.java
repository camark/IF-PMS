package com.xnck.ifpms.service;

import java.util.ArrayList;
import java.util.List;

import org.nutz.dao.Cnd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xnck.ifpms.dao.ResourceDao;
import com.xnck.ifpms.entity.Resource;

/**
 * 权限验证业务逻辑
 * @author zhangmengliang
 *
 */
@Service
public class PermissionService {
	
	@Autowired
	private ResourceDao resourceDao;

	/**
	 * 检查人员是否可以访问某个资源
	 * @param userId 被验证的人员ID
	 * @param resourceCode 资源代码
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月4日
	 */
	public Boolean getCanRequestResource(String userId, String resourceCode){
		List<Resource> resources = resourceDao.getResourcesForUserRoleAction(userId, Cnd.where(Resource.FIELD_CODE, "=", resourceCode));
		if (resources != null && resources.size() > 0) {
			return true;
		}
		resources = resourceDao.getResourcesForUserAction(userId, Cnd.where(Resource.FIELD_CODE, "=", resourceCode));
		if (resources != null && resources.size() > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * 根据视图名称获得某个人员有权限访问的资源
	 * @param userId 人员ID
	 * @param viewName 视图名称
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月4日
	 */
	public List<Resource> getCanRequestResourcesForView(String userId, String viewName){
		List<Resource> returnResources = new ArrayList<Resource>();
		//根据人员所属角色获得可以访问的资源
		List<Resource> resources = resourceDao.getResourcesForUserRoleAction(userId, Cnd.where(Resource.FIELD_VIEWNAME, "=", viewName));
		returnResources.addAll(resources);
		//根据人员直接获得可以访问的资源
		resources = resourceDao.getResourcesForUserAction(userId, Cnd.where(Resource.FIELD_VIEWNAME, "=", viewName));
		returnResources.addAll(resources);
		return returnResources;
	}
}
