package com.xnck.ifpms.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xiaoleilu.hutool.StrUtil;
import com.xnck.ifpms.annotation.Permission;
import com.xnck.ifpms.entity.Resource;
import com.xnck.ifpms.service.PermissionService;
import com.xnck.ifpms.utils.ContextUtils;
import com.xnck.ifpms.utils.SessionUtils;


/**
 * 权限拦截器
 * @author zhangmengliang
 *
 */
public class PermisInterceptor  extends HandlerInterceptorAdapter{
	protected Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private PermissionService permissionService;
	
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
        
    }
 
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view) throws Exception {
        //如果返回的视图不为NULL，则查找该视图下当前登录人员能够访问的资源，并装载至视图中返回
    	if (view != null) {
        	String userId = this.getCurrentUserId(request);
        	if (StrUtil.isNotBlank(userId) && StrUtil.isNotBlank(view.getViewName())) {
        		List<Resource> resources = permissionService.getCanRequestResourcesForView(userId, view.getViewName().toLowerCase());
	    		for (Resource resource : resources) {
	    			view.addObject(resource.getCode(), true);
	    		}
			}
        }
    }
   
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	//如果拦截的是方法，检查是否有Permission注解，如果有注解则验证是否登录，再验证是否有权限访问注解内标识的资源
    	if (handler instanceof HandlerMethod) {
    		HandlerMethod method = (HandlerMethod)handler;
        	Permission permission = method.getMethodAnnotation(Permission.class);
        	if (null == permission || !StrUtil.isNotBlank(permission.value())) {
    			return true;
    		}
        	String userId = this.getCurrentUserId(request);
        	if (StrUtil.isNotBlank(userId)) {
				Boolean canRequest = permissionService.getCanRequestResource(userId, permission.value());
				if (canRequest) {
					return true;
				}
			}
        	ContextUtils.respString(response, "无权限");
        	return false;
    	}
    	return true;
    }
    
    private String getCurrentUserId(HttpServletRequest request){
    	String userId;
    	try {
    		 userId = SessionUtils.getCurrentUserId(request);
		} catch (Exception e) {
			logger.error(e.toString());
			userId = null;
		}
    	return userId;
    }
}
