package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.ResourceType;

/**
 * 资源种类数据处理
 * @author zhangmengliang
 *
 */
@Component
public class ResourceTypeDao extends BaseDao<ResourceType>{

}
