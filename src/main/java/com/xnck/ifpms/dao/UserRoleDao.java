package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.UserRole;

/**
 * 人员和角色对应关系的数据处理
 * @author zhangmengliang
 *
 */
@Component
public class UserRoleDao extends BaseDao<UserRole>{

}
