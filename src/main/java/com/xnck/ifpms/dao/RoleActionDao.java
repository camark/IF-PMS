package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.RoleAction;

/**
 * 角色和权限关系数据处理
 * @author zhangmengliang
 *
 */
@Component
public class RoleActionDao extends BaseDao<RoleAction>{

}
