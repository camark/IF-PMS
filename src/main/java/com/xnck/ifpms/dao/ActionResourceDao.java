package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.ActionResource;

/**
 * 权限和资源对应关系的数据处理
 * @author zhangmengliang
 *
 */
@Component
public class ActionResourceDao extends BaseDao<ActionResource>{

}
