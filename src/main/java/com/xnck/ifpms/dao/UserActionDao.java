package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.UserAction;

/**
 * 人员和权限对应关系数据处理
 * @author zhangmengliang
 *
 */
@Component
public class UserActionDao extends BaseDao<UserAction>{

}
