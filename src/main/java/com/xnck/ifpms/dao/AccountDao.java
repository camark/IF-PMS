package com.xnck.ifpms.dao;

import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.Account;

/**
 * 账户数据处理
 * @author zhangmengliang
 *
 */
@Component
public class AccountDao extends BaseDao<Account>{

}
