package com.xnck.ifpms.dao;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.Role;

/**
 * 角色数据处理
 * @author zhangmengliang
 *
 */
@Component
public class RoleDao extends BaseDao<Role>{
	
	/**
	 * 获得人员所具有的角色总数量
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountRolesForUser(String userId, Condition condition){
		Sql sql = dao.sqls().create("roleDao.getCountRolesForUser");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}

	/**
	 * 分页获得人员所具有的角色
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @param currentPage 当前页
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Role> getRolesForUser(String userId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("roleDao.getRolesForUser");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得人员所不具有的角色总数量
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountRolesNotForUser(String userId, Condition condition){
		Sql sql = dao.sqls().create("roleDao.getCountRolesNotForUser");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}
	
	/**
	 * 分页获得人员所不具有的角色
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Role> getRolesNotForUser(String userId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("roleDao.getRolesNotForUser");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得权限所具有的角色总数量
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountRolesForAction(String actionId, Condition condition){
		Sql sql = dao.sqls().create("roleDao.getCountRolesForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}

	/**
	 * 分页获得权限所具有的角色
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Role> getRolesForAction(String actionId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("roleDao.getRolesForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得权限所不具有的角色总数量
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountRolesNotForAction(String actionId, Condition condition){
		Sql sql = dao.sqls().create("roleDao.getCountRolesNotForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}
	
	/**
	 * 分页获得权限所不具有的角色总数量
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Role> getRolesNotForAction(String actionId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("roleDao.getRolesNotForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
}
