package com.xnck.ifpms.dao;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.User;

/**
 * 人员数据处理
 * @author zhangmengliang
 *
 */
@Component
public class UserDao extends BaseDao<User>{

	/**
	 * 获得角色所包含的人员总数量
	 * @param roleId 角色ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountUsersForRole(String roleId, Condition condition){
		Sql sql = dao.sqls().create("userDao.getCountUsersForRole");
		sql.params().set("roleid", roleId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}

	/**
	 * 分页获得角色所包含的人员
	 * @param roleId 角色ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<User> getUsersForRole(String roleId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("userDao.getUsersForRole");
		sql.params().set("roleid", roleId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得角色未包含的人员总数量
	 * @param roleId 角色ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountUsersNotForRole(String roleId, Condition condition){
		Sql sql = dao.sqls().create("userDao.getCountUsersNotForRole");
		sql.params().set("roleid", roleId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}
	
	/**
	 * 分页获得角色未包含的人员
	 * @param roleId 角色ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<User> getUsersNotForRole(String roleId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("userDao.getUsersNotForRole");
		sql.params().set("roleid", roleId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
}
