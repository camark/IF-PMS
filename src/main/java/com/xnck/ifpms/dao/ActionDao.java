package com.xnck.ifpms.dao;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.Action;

/**
 * 权限数据处理
 * @author zhangmengliang
 *
 */
@Component
public class ActionDao extends BaseDao<Action>{
	
	/**
	 * 获得人员所拥有的权限
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Action> getActionsForUser(String userId, Condition condition){
		Sql sql = dao.sqls().create("actionDao.getActionsForUser");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
		return this.search(sql);
	}
	
	/**
	 * 获得角色所拥有的权限
	 * @param roleId 角色ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Action> getActionsForRole(String roleId, Condition condition){
		Sql sql = dao.sqls().create("actionDao.getActionsForRole");
		sql.params().set("roleid", roleId);
		sql.setCondition(condition);
		return this.search(sql);
	}
	
	/**
	 * 获得资源所对应的权限
	 * @param resId 资源ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Action> getActionsForResource(String resId, Condition condition){
		Sql sql = dao.sqls().create("actionDao.getActionsForResource");
		sql.params().set("resourceid", resId);
		sql.setCondition(condition);
		return this.search(sql);
	}
}
