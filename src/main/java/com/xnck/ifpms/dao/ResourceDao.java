package com.xnck.ifpms.dao;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.sql.Sql;
import org.springframework.stereotype.Component;

import com.xnck.ifpms.entity.Resource;

/**
 * 资源数据处理
 * @author zhangmengliang
 *
 */
@Component
public class ResourceDao extends BaseDao<Resource>{
	/**
	 * 获得权限所具有的资源总数量
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountResourcesForAction(String actionId, Condition condition){
		Sql sql = dao.sqls().create("resourceDao.getCountResourcesForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}

	/**
	 * 分页获得权限所具有的资源
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Resource> getResourcesForAction(String actionId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("resourceDao.getResourcesForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得权限所不具有的资源总数量
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public Integer getCountResourcesNotForAction(String actionId, Condition condition){
		Sql sql = dao.sqls().create("resourceDao.getCountResourcesNotForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
		return this.searchCount(sql);
	}
	
	/**
	 * 分页获得权限所不具有的资源
	 * @param actionId 权限ID
	 * @param condition 查询条件
	 * @param currentPage 当前页码
	 * @param pageSize 页大小
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年10月25日
	 */
	public List<Resource> getResourcesNotForAction(String actionId, Condition condition, Integer currentPage, Integer pageSize){
		Sql sql = dao.sqls().create("resourceDao.getResourcesNotForAction");
		sql.params().set("actionid", actionId);
		sql.setCondition(condition);
	    return this.searchByPage(sql, currentPage, pageSize);
	}
	
	/**
	 * 获得人员所属角色对应的权限具有的资源
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月1日
	 */
	public List<Resource> getResourcesForUserRoleAction(String userId, Condition condition){
		Sql sql = dao.sqls().create("resourceDao.getResourcesForUserRoleAction");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
	    return this.search(sql);
	}
	
	/**
	 * 获得人员对应的权限具有的资源
	 * @param userId 人员ID
	 * @param condition 查询条件
	 * @return
	 * @author:zhangmengliang
	 * @date: 2015年11月1日
	 */
	public List<Resource> getResourcesForUserAction(String userId, Condition condition){
		Sql sql = dao.sqls().create("resourceDao.getResourcesForUserAction");
		sql.params().set("userid", userId);
		sql.setCondition(condition);
	    return this.search(sql);
	}
}
