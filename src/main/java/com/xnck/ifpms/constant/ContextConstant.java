package com.xnck.ifpms.constant;

/**
 * 上下文常量
 * @author zhangmengliang
 *
 */
public class ContextConstant {

	/** 人员身份凭证键名 **/
	public static final String IDEN_CERT_KEY = "current_user_cert";
}
