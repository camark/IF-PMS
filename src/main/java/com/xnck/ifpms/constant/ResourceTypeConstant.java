package com.xnck.ifpms.constant;

/**
 * 资源类型常量
 * @author zhangmengliang
 *
 */
public class ResourceTypeConstant {

	public final static String URL = "1";
	public final static String HTML = "2";
}
