IF-PMS
===
权限管理系统的雏形，用来熟悉J2EE的几个开源框架的学习项目，如果用来作为中小型管理系统的基础构建也是可行的。<br>
WEB应用框架采用SpringMVC。<br>
ORM框架采用Nutz.Dao。<br>
模板引擎采用Bteel。<br>
前端使用H-ui.admin。<br>
演示地址:http://blackzs.com:9191/