/*
Navicat MySQL Data Transfer

Source Server         : local.mysql
Source Server Version : 50622
Source Host           : localhost:3306
Source Database       : ifpms

Target Server Type    : MYSQL
Target Server Version : 50622
File Encoding         : 65001

Date: 2016-07-01 10:03:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_account
-- ----------------------------
DROP TABLE IF EXISTS `t_account`;
CREATE TABLE `t_account` (
  `id` varchar(50) NOT NULL COMMENT '主健',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `userid` varchar(50) DEFAULT NULL COMMENT '用户信息ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_account
-- ----------------------------
INSERT INTO `t_account` VALUES ('1', 'admin', '123456', 'admin');
INSERT INTO `t_account` VALUES ('2', 'test', '123456', 'test');
INSERT INTO `t_account` VALUES ('72178cfd-c621-461a-b964-a1a0afe65072', 'user', '123456', 'b95e3f9a-2f2c-4ffc-8160-1a4962081c6b');
INSERT INTO `t_account` VALUES ('a668880b-c873-43ba-92db-53ba52dc9e3f', 'searcher', '123456', '69bb2e09-400d-4b0a-b575-adb43cbc614e');
INSERT INTO `t_account` VALUES ('eb8119e7-3827-4538-a1b4-99e6d14035af', 'wangwu', '123456', '737eccc8-e0fa-475e-99fb-d396be282672');

-- ----------------------------
-- Table structure for t_action_parent_track
-- ----------------------------
DROP TABLE IF EXISTS `t_action_parent_track`;
CREATE TABLE `t_action_parent_track` (
  `parentid` varchar(50) NOT NULL,
  `childid` varchar(50) NOT NULL,
  PRIMARY KEY (`parentid`,`childid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_action_parent_track
-- ----------------------------

-- ----------------------------
-- Table structure for t_action_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_action_resource`;
CREATE TABLE `t_action_resource` (
  `actionid` varchar(50) NOT NULL,
  `resourceid` varchar(50) NOT NULL,
  PRIMARY KEY (`actionid`,`resourceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_action_resource
-- ----------------------------
INSERT INTO `t_action_resource` VALUES ('61dc62df-5523-4db9-b8bc-c992b5ba4a26', 'a85277a7-63d4-4830-90ee-46d999383bae');
INSERT INTO `t_action_resource` VALUES ('a26d71f9-4b18-473e-b0ff-e4f51006b519', '90e7b2ef-06bc-48c6-8b28-75435d316bcb');
INSERT INTO `t_action_resource` VALUES ('a26d71f9-4b18-473e-b0ff-e4f51006b519', 'a85277a7-63d4-4830-90ee-46d999383bae');

-- ----------------------------
-- Table structure for t_info_action
-- ----------------------------
DROP TABLE IF EXISTS `t_info_action`;
CREATE TABLE `t_info_action` (
  `id` varchar(50) NOT NULL,
  `displayname` varchar(20) DEFAULT NULL,
  `parentid` varchar(50) DEFAULT NULL,
  `createrid` varchar(50) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_action
-- ----------------------------
INSERT INTO `t_info_action` VALUES ('61dc62df-5523-4db9-b8bc-c992b5ba4a26', '查看', '7f3a2518-72c5-4a1e-923c-e37c6867227a', null, '', '', '2015-11-01 22:58:07');
INSERT INTO `t_info_action` VALUES ('7f3a2518-72c5-4a1e-923c-e37c6867227a', '权限测试', '0', null, '', '', '2015-11-01 22:57:47');
INSERT INTO `t_info_action` VALUES ('a26d71f9-4b18-473e-b0ff-e4f51006b519', '使用', '7f3a2518-72c5-4a1e-923c-e37c6867227a', null, '', '', '2015-11-01 22:58:19');

-- ----------------------------
-- Table structure for t_info_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_info_menu`;
CREATE TABLE `t_info_menu` (
  `id` varchar(50) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `displayname` varchar(20) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `parentid` varchar(50) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `orderno` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_menu
-- ----------------------------

-- ----------------------------
-- Table structure for t_info_resource
-- ----------------------------
DROP TABLE IF EXISTS `t_info_resource`;
CREATE TABLE `t_info_resource` (
  `id` varchar(50) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `displayname` varchar(50) DEFAULT NULL,
  `typeid` varchar(50) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `createrid` varchar(255) DEFAULT NULL,
  `viewname` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_resource
-- ----------------------------
INSERT INTO `t_info_resource` VALUES ('90e7b2ef-06bc-48c6-8b28-75435d316bcb', 'testPermissionText', '权限文本框', '22397131-7542-11e5-8cd2-8561568ab5ec', '2015-11-01 23:04:20', '', '', null, '/test/testpermission');
INSERT INTO `t_info_resource` VALUES ('a85277a7-63d4-4830-90ee-46d999383bae', 'url_test_testpermission', '权限测试', '221dc364-7542-11e5-8cd2-8561568ab5ec', '2015-11-01 22:57:26', '', '', null, '/test/testpermission');

-- ----------------------------
-- Table structure for t_info_role
-- ----------------------------
DROP TABLE IF EXISTS `t_info_role`;
CREATE TABLE `t_info_role` (
  `id` varchar(50) NOT NULL,
  `displayname` varchar(20) DEFAULT NULL,
  `createrid` varchar(50) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_role
-- ----------------------------
INSERT INTO `t_info_role` VALUES ('78d53b4b-8d56-43a8-833a-a78bee3ad99c', '使用者', null, '', '2015-11-01 22:59:22');
INSERT INTO `t_info_role` VALUES ('eda7fe6c-8210-4cab-b584-66a624346e2e', '查询者', null, '', '2015-11-01 22:59:16');

-- ----------------------------
-- Table structure for t_info_user
-- ----------------------------
DROP TABLE IF EXISTS `t_info_user`;
CREATE TABLE `t_info_user` (
  `id` varchar(50) NOT NULL,
  `displayname` varchar(50) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  `createrid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_user
-- ----------------------------
INSERT INTO `t_info_user` VALUES ('69bb2e09-400d-4b0a-b575-adb43cbc614e', 'searcher', '', '2015-11-01 22:59:54', null);
INSERT INTO `t_info_user` VALUES ('b95e3f9a-2f2c-4ffc-8160-1a4962081c6b', 'user', '', '2015-11-01 23:00:11', null);

-- ----------------------------
-- Table structure for t_resource_type
-- ----------------------------
DROP TABLE IF EXISTS `t_resource_type`;
CREATE TABLE `t_resource_type` (
  `id` varchar(50) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_resource_type
-- ----------------------------
INSERT INTO `t_resource_type` VALUES ('221dc364-7542-11e5-8cd2-8561568ab5ec', 'url', 'URL');
INSERT INTO `t_resource_type` VALUES ('222bdd33-7542-11e5-8cd2-8561568ab5ec', 'page', '页面');
INSERT INTO `t_resource_type` VALUES ('22397131-7542-11e5-8cd2-8561568ab5ec', 'tag', 'HTML元素');

-- ----------------------------
-- Table structure for t_role_action
-- ----------------------------
DROP TABLE IF EXISTS `t_role_action`;
CREATE TABLE `t_role_action` (
  `roleid` varchar(50) NOT NULL,
  `actionid` varchar(50) NOT NULL,
  PRIMARY KEY (`roleid`,`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_role_action
-- ----------------------------
INSERT INTO `t_role_action` VALUES ('3258031c-a104-4c31-98a2-92a1f933138b', '52a82ca6-dcba-45bd-a34f-4354fa43a3be');
INSERT INTO `t_role_action` VALUES ('3dd4ba66-2921-4df6-88a4-d01f028a341c', '52a82ca6-dcba-45bd-a34f-4354fa43a3be');
INSERT INTO `t_role_action` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', '0');
INSERT INTO `t_role_action` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', '490319ef-73d4-4a84-95da-a3f3b29e271e');
INSERT INTO `t_role_action` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', '9ce79fa3-9ed8-41f0-99de-c803bccdb93f');
INSERT INTO `t_role_action` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', 'b3ea382f-19a8-42b4-a2f1-b45f82ff9f7d');
INSERT INTO `t_role_action` VALUES ('78d53b4b-8d56-43a8-833a-a78bee3ad99c', '0');
INSERT INTO `t_role_action` VALUES ('78d53b4b-8d56-43a8-833a-a78bee3ad99c', '7f3a2518-72c5-4a1e-923c-e37c6867227a');
INSERT INTO `t_role_action` VALUES ('78d53b4b-8d56-43a8-833a-a78bee3ad99c', 'a26d71f9-4b18-473e-b0ff-e4f51006b519');
INSERT INTO `t_role_action` VALUES ('eda7fe6c-8210-4cab-b584-66a624346e2e', '0');
INSERT INTO `t_role_action` VALUES ('eda7fe6c-8210-4cab-b584-66a624346e2e', '61dc62df-5523-4db9-b8bc-c992b5ba4a26');
INSERT INTO `t_role_action` VALUES ('eda7fe6c-8210-4cab-b584-66a624346e2e', '7f3a2518-72c5-4a1e-923c-e37c6867227a');

-- ----------------------------
-- Table structure for t_user_action
-- ----------------------------
DROP TABLE IF EXISTS `t_user_action`;
CREATE TABLE `t_user_action` (
  `userid` varchar(50) NOT NULL,
  `actionid` varchar(50) NOT NULL,
  PRIMARY KEY (`userid`,`actionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_action
-- ----------------------------
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', '0');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', '29b22aab-0f42-4f8e-8330-59d132608e30');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', '86edd8d4-82f9-42c7-8ecd-fc3f0953ea6e');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', '9b835ddc-b9ec-48af-b2aa-a6881ac694a7');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', '9ce79fa3-9ed8-41f0-99de-c803bccdb93f');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', 'b3ea382f-19a8-42b4-a2f1-b45f82ff9f7d');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', 'bae769b2-1e61-4e17-97c5-1b1d90e20853');
INSERT INTO `t_user_action` VALUES ('737eccc8-e0fa-475e-99fb-d396be282672', 'c88a4196-a238-4803-8c12-b226fa478cd7');

-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role` (
  `roleid` varchar(50) NOT NULL,
  `userid` varchar(50) NOT NULL,
  PRIMARY KEY (`roleid`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------
INSERT INTO `t_user_role` VALUES ('3258031c-a104-4c31-98a2-92a1f933138b', '4c98a4a6-0081-4dcb-8129-7a0930145759');
INSERT INTO `t_user_role` VALUES ('3dd4ba66-2921-4df6-88a4-d01f028a341c', '4c98a4a6-0081-4dcb-8129-7a0930145759');
INSERT INTO `t_user_role` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', '9b2201c8-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', '9b2f01ba-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('66c66fb3-7e7e-4929-be6f-ee651160ae42', 'e0c9fdfb-547f-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('78d53b4b-8d56-43a8-833a-a78bee3ad99c', 'b95e3f9a-2f2c-4ffc-8160-1a4962081c6b');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '734e22a6-40b9-48b5-a84f-3f291649a2b2');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '737eccc8-e0fa-475e-99fb-d396be282672');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '9b3f2b76-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '9b4ad616-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '9b557f44-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('cf24adc7-1285-4851-94df-b43f257300ed', '9b617aa8-57c6-11e5-aa0c-fb5acf0091d9');
INSERT INTO `t_user_role` VALUES ('eda7fe6c-8210-4cab-b584-66a624346e2e', '69bb2e09-400d-4b0a-b575-adb43cbc614e');

-- ----------------------------
-- View structure for v_info_action
-- ----------------------------
DROP VIEW IF EXISTS `v_info_action`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_info_action` AS SELECT
a.id,
a.displayname,
a.parentid,
a.createrid,
a.`enable`,
a.remark,
a.addtime,
(select count(*) from t_info_action where parentid = a.id) as childcount
FROM
t_info_action a ;

-- ----------------------------
-- View structure for v_info_resource
-- ----------------------------
DROP VIEW IF EXISTS `v_info_resource`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_info_resource` AS SELECT
a.*, b.`name` AS typename, b.`code` as typecode
FROM
t_info_resource a
INNER JOIN t_resource_type b ON a.typeid = b.id ;
